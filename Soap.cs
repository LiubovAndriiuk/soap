﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace WebServiseCreat
{
    class Soap
    {
        public HttpWebRequest CreateSoapWebRequest()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.dneonline.com/calculator.asmx");
            request.Headers.Add(@"SOAPAction:http://tempuri.org/Subtract");
            request.ContentType = "text/xml; charset=utf-8";
            request.Method = "POST";
            return request;
        }
        public void SubtractNumber(int a, int b)
        {
            var request = CreateSoapWebRequest();
            XmlDocument body = new XmlDocument();
            var bodyXml = @"<?xml version =""1.0"" encoding=""utf-8""?>" +
            @"<soap:Envelope xmlns:xsi =""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd =""http://www.w3.org/2001/XMLSchema"" xmlns:soap = ""http://schemas.xmlsoap.org/soap/envelope/"">" +
            @"<soap:Body>" +
            @"<Subtract xmlns = ""http://tempuri.org/"">" +
            $"<intA>{a}</intA>" +
            $"<intB>{b}</intB>" +
           "</Subtract>" +
           "</soap:Body>" +
           "</soap:Envelope>";
            body.LoadXml(bodyXml);
            using (Stream stream =request.GetRequestStream())
            {
                body.Save(stream);
            }

            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(Serviceres.GetResponseStream()))
                {
                    var serviceResult = reader.ReadToEnd();
                    var doc = XDocument.Parse(serviceResult);
                    XNamespace ns = "http://tempuri.org/";
                    var result = doc.Root.Descendants(ns + "SubtractResponse").Elements(ns + "SubtractResult").FirstOrDefault();
                    Console.WriteLine(result.Value);
                }

            }

        }
    }
}
