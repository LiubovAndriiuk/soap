﻿using NUnit.Framework;


namespace WebServiseCreat
{
    class UserTest
    {
       

        [Test]
        public void GetAllUsers()
        {
            var repository = new UserRepository();
            var user = new UserData() { Name = "Joch", Job = "QA" };
            repository.CreateUser(user);
            string eveId = repository.GetUserByIDEmail("eve.holt@reqres.in");
            repository.DeleteUser(eveId);
        }
        [Test]
        public void PutUsers()
        {
            var repository = new UserRepository();
            var user = new UserData() { NameUpdate = "ALex", JobUpdate = "Dev" };
            repository.CreateUser(user);
            repository.UpDateUser("janet.weaver@reqres.in");
        }
        [Test]
        public void DeleteUpdateUser()
        {
            var repository = new UserRepository();
            var user = new UserData() { NameUpdate = "ALex", JobUpdate = "Dev" };
            repository.CreateUser(user);
            repository.UpDateUser("2");
            repository.DeleteUser("2");

        }
    }
}
