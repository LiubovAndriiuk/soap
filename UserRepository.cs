﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using System;
using System.Net;

namespace WebServiseCreat
{
    public class UserRepository
    {
        private string _baseUrl = "https://reqres.in/api/users";
        
        public string GetUserByIDEmail(string email)
        {
        RestClient restClient = new RestClient(_baseUrl);
        RestRequest request = new RestRequest();
        request.Method= Method.GET;
            var response = restClient.Execute(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            var jObject = JObject.Parse(response.Content);
            string contactId = jObject.SelectToken($"$..data[?(@.email == '{email}')].id").ToString();
            return contactId;
        }


        public void CreateUser(UserData data)
        {
            RestClient restClient = new RestClient(_baseUrl);
            RestRequest request = new RestRequest();
            request.Method = Method.POST;
            request.AddParameter("application/json", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
            var responce = restClient.Execute(request);
            Assert.IsTrue(responce.StatusCode == HttpStatusCode.Created);
        }

        public void UpDateUser(string id)
        {
            UserData updateUserData = new UserData();
            RestClient restClient = new RestClient($"{_baseUrl}/{id}");
            RestRequest request = new RestRequest();
            request.Method = Method.PUT;
            request.AddParameter("application/json", JsonConvert.SerializeObject(updateUserData), ParameterType.RequestBody);
            var responce = restClient.Execute(request);
            Assert.IsFalse(responce.StatusCode == HttpStatusCode.Created);

        }

        public void DeleteUser(string id)
        {
            RestClient restClient = new RestClient($"{_baseUrl}/{id}");
            RestRequest request = new RestRequest();
            request.Method = Method.DELETE;
            var responce = restClient.Execute(request);
            Assert.IsTrue(responce.StatusCode == HttpStatusCode.NoContent);
        }
    }
}
